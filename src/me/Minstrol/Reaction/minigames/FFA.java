package me.Minstrol.Reaction.minigames;

import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class FFA implements Listener {

    static Location arena = new Location(Bukkit.getWorld("world"), 0, 2, 0, 0, 90);
    static int countdown = 15;
    static String prefix = ChatColor.RED + "[Reaction] " + ChatColor.YELLOW;
    static ArrayList<String> FFAPlayers = new ArrayList<String>();
    public static Connection connection;

    public static void start(final Player player) {
        FFAPlayers.add(player.getName());
        Bukkit.getServer().broadcastMessage(ChatColor.BOLD + "" + ChatColor.BLUE + "--------------------------------------------");
        Bukkit.getServer().broadcastMessage(ChatColor.AQUA + "" + ChatColor.BOLD + "                 FFA                          ");
        Bukkit.getServer().broadcastMessage("                                                      ");
        Bukkit.getServer().broadcastMessage(ChatColor.YELLOW + "   Wellcome to reaction! What better to do then     ");
        Bukkit.getServer().broadcastMessage(ChatColor.YELLOW + "          have a quick FFA warm up :D               ");
        Bukkit.getServer().broadcastMessage(ChatColor.BOLD + "" + ChatColor.BLUE + "--------------------------------------------");
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.hidePlayer(player);
            p.getInventory().clear();
            p.teleport(arena);
            p.setFlying(false);
        }
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("Reaction"), new Runnable() {
            @Override
            public void run() {
                if (countdown != 0) {
                    if (countdown != -1) {
                        Bukkit.getServer().broadcastMessage(prefix + "The game is starting in " + countdown);
                        Bukkit.getWorld("world").playSound(player.getLocation(), Sound.CLICK, 5, 10);
                        countdown--;
                    }
                } else {
                    countdown--;
                    ItemStack bow = new ItemStack(Material.BOW, 1);
                    ItemMeta meta = bow.getItemMeta();
                    meta.setDisplayName(ChatColor.GOLD + "The start up bow");
                    Bukkit.getWorld("world").playSound(player.getLocation(), Sound.LEVEL_UP, 5, 10);
                    meta.addEnchant(Enchantment.ARROW_DAMAGE, 100, true);
                    bow.setItemMeta(meta);
                    Bukkit.getServer().broadcastMessage(prefix + "The game has begun!");
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        p.getInventory().addItem(bow);
                        p.showPlayer(player);
                    }
                    openConnection();
                    try {
                        PreparedStatement newPlayer = connection.prepareStatement("INSERT INTO `reaction`(`player`, `kills`, `deaths`, `wins`) VALUES (?,0,0,0);");
                        newPlayer.setString(1, player.getName());
                        newPlayer.close();
                    } catch (Exception e){
                        e.printStackTrace();
                    } finally {
                        closeConnection();
                    }

                }

            }
        }, 0L, 20L);
    }

    public synchronized static void openConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://66.85.144.162:3306/mcph465133", "mcph465133", "6d797f967b");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static boolean playerDataContainsPlayer(Player player) {
        try {
            PreparedStatement sql = connection.prepareStatement("SELECT * FROM `reaction` WHERE player=?;");
            sql.setString(1, player.getName());
            ResultSet resultSet = sql.executeQuery();
            boolean containsPlayer = resultSet.next();

            sql.close();
            resultSet.close();

            return containsPlayer;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @EventHandler
    public void login(PlayerLoginEvent event) {
        openConnection();
        try {
            int previouskills = 0;
            if (playerDataContainsPlayer(event.getPlayer())) {
                PreparedStatement statement = connection.prepareStatement("SELECT kills FROM `player_data` WHERE player=?;");
                statement.setString(1, event.getPlayer().getName());
                ResultSet resultSet = statement.executeQuery();
                resultSet.next();

                previouskills = resultSet.getInt("kills");

                PreparedStatement loginsUpdate = connection.prepareStatement("UPDATE `reaction` SET kills=? WHERE player=?");
                loginsUpdate.setInt(1, previouskills);
                loginsUpdate.setString(2, event.getPlayer().getName());
                loginsUpdate.executeUpdate();

                loginsUpdate.close();
                statement.close();
                resultSet.close();
            } else {
                PreparedStatement newPlayer = connection.prepareStatement("INSERT INTO `reaction`(`player`, `kills`, `deaths`, `wins`) VALUES (?,0,0,0);");
                newPlayer.setString(1, event.getPlayer().getName());
                //newPlayer.execute();
                newPlayer.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    @EventHandler
    public void killed(PlayerDeathEvent event) {
        Player player = (Player) event.getEntity();
        if (FFAPlayers.contains(player.getName())) {
            openConnection();
            int deaths = 0;
            try {
                PreparedStatement deathtotheplayer = connection.prepareStatement("SELECT deaths FROM `reaction` WHERE player=?;");
                deathtotheplayer.setString(1, player.getName());
                ResultSet deathR = deathtotheplayer.executeQuery();
                deathR.next();

                deaths = deathR.getInt("deaths");

                PreparedStatement deathsUpdate = connection.prepareStatement("UPDATE `reaction` SET deaths=? WHERE player=?");
                deathsUpdate.setInt(1, deaths + 1);
                deathsUpdate.setString(2, player.getName());
                deathsUpdate.executeUpdate();

                deathR.close();
                deathsUpdate.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeConnection();
            }
        }
    }

    @EventHandler
    public void kill(EntityDamageByEntityEvent event) {
        Player player = (Player) event.getEntity();
        if (FFAPlayers.contains(player.getName())) {
            if (player.isDead()) {
                openConnection();
                int kills = 0;
                try {
                    PreparedStatement deathtotheplayer = connection.prepareStatement("SELECT kills FROM `reaction` WHERE player=?;");
                    deathtotheplayer.setString(1, player.getName());
                    ResultSet deathR = deathtotheplayer.executeQuery();
                    deathR.next();

                    kills = deathR.getInt("kills");

                    PreparedStatement deathsUpdate = connection.prepareStatement("UPDATE `reaction` SET kills=? WHERE player=?");
                    deathsUpdate.setInt(1, kills + 1);
                    deathsUpdate.setString(2, player.getName());
                    deathsUpdate.executeUpdate();

                    deathR.close();
                    deathsUpdate.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    closeConnection();
                }
            }
        }
    }
}
