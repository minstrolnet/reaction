package me.Minstrol.Reaction;

import me.Minstrol.Reaction.Listeners.Events;
import me.Minstrol.Reaction.minigames.FFA;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;


public class Main extends JavaPlugin implements Listener {

    Location waitingLobby = new Location(Bukkit.getWorld("world"), 10, 10, 10, 0, 90);
    int countdown = 10;
    String prefix = ChatColor.RED + "[Reaction] " + ChatColor.YELLOW;
    ArrayList<Player> onlinePlayers = new ArrayList<Player>();

    @Override
    public void onEnable() {
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
        Bukkit.getServer().getPluginManager().registerEvents(new FFA(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new Events(), this);
    }

    @EventHandler
    public void join(PlayerJoinEvent event){
        final Player player = (Player) event.getPlayer();
        player.setGameMode(GameMode.ADVENTURE);
        onlinePlayers.add(player);
        Bukkit.getServer().broadcastMessage(player.getName() + " has joined the game!");
        player.teleport(waitingLobby);
        if (onlinePlayers.size() > 2){
            Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
                @Override
                public void run() {
                    if (countdown != 0) {
                        if (countdown > -1) {
                            Bukkit.getServer().broadcastMessage(prefix + "The game is starting in " + countdown);
                            Bukkit.getWorld("world").playSound(player.getLocation(), Sound.CLICK, 5, 10);
                            countdown--;
                        }
                    } else {
                        Bukkit.getServer().broadcastMessage(prefix + "The game has begun!");
                        Bukkit.getWorld("world").playSound(player.getLocation(), Sound.LEVEL_UP, 5, 10);
                        for (Player p : onlinePlayers) {
                            FFA.start(p);
                        }
                        countdown--;
                    }
                }
            }, 0L, 20L);
        }
    }
}
